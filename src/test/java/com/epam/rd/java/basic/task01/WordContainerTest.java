package com.epam.rd.java.basic.task01;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.util.Arrays;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;

import com.epam.rd.java.basic.TestHelper;

public class WordContainerTest {

	private static final InputStream STD_IN = System.in;

	private static final PrintStream STD_OUT = System.out;

	private static final String TEST_DATA_FILE_ENCODINE = "UTF-8";

	private static final String ARGS_SEPARATOR = "|";

	private static final String EOL_CHAR = "~";

	private final ByteArrayOutputStream baos = new ByteArrayOutputStream();

	private final PrintStream out = new PrintStream(baos);

	@ParameterizedTest
	@CsvFileSource(resources = "data.csv", numLinesToSkip = 2, delimiter = ';')
	void shouldPrintExpectedOutput(String argsLine, String inputLine, String expectedLine) 
					throws UnsupportedEncodingException {
		
		String[] args = argsLine != null ? argsLine.split('\\' + ARGS_SEPARATOR) : null;
			
		String input = inputLine.replace(EOL_CHAR, System.lineSeparator());
		String expected = expectedLine.replace(EOL_CHAR, System.lineSeparator());

		byte[] buf = input.getBytes(TEST_DATA_FILE_ENCODINE);
		ByteArrayInputStream bais = new ByteArrayInputStream(buf);
		
		System.setOut(out);
		System.setIn(bais);

		try {
			WordContainer.main(args);

			out.flush();
			String actual = baos.toString();

			TestHelper.logToStdErr(expected, actual);
			assertEquals(expected, actual);
		} finally {
			System.setOut(STD_OUT);
			System.setIn(STD_IN);
		}
	}

}
